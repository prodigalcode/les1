var path = require('path');
var webpack = require('webpack');
var merge = require('webpack-merge');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const TARGET = process.env.npm_lifecycle_event;
var buildPath = path.join(__dirname, 'build');
console.log(buildPath);

var common = {
    entry: {
        app: ["./src/app.js"],
        vendors: ["d3"]
    },
    output: {
        path: buildPath,
        filename: "app.js"
    },
    resolve: {
        extensions: ['', ".js", ".css", ".styl", ".html", ".json"]
    },
    module: {
        loaders: [
            // {
            //     test: /\.js$/,
            //     loader: 'babel',
            //     exclude: /node_modules/,
            //     query: {
            //         presets: ['es2015']
            //     }
            // },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!stylus-loader")
            },
            {
                test: /\.(html)|(json)$/,
                exclude: /node_modules/,
                loader: "file?name=[name].[ext]"
            }
        ]
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        // new HtmlWebpackPlugin(/*{ template: 'src/index.html' }*/),
        new ExtractTextPlugin("style.css"),
        new webpack.ProvidePlugin({ d3: "d3" }),
        new webpack.optimize.CommonsChunkPlugin('vendors','vendors.js')
    ]
};

if(TARGET === 'start' || !TARGET) {
    module.exports = merge(common, {
        devtool: 'source-map',
        watch: true,
        devServer: {
            contentBase: buildPath,
            historyApiFallback: true,
            hot: true,
            inline: true,
            progress: true,
            stats: 'errors-only',
            host: "localhost",
            port: "3000"
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ]
    });
}
else if(TARGET === 'build') {
    module.exports = merge(common, {
        plugins: [
            new webpack.optimize.UglifyJsPlugin()
        ]
    });
}