require("./css/index.css");
require("./html/index.html");

var points = []
    , isDragging = false
    , canvas = document.createElement("canvas") // HTMLCanvasElement
    , ctx = canvas.getContext("2d")
    , rgb = [rand(255, 0), rand(255, 0), rand(255, 0)]
    , w, h
    , step = 30
    ;

function resize() {
    w = document.body.clientWidth;
    h = document.body.clientHeight;

    if (canvas.width !== w) {
        canvas.width = w;
    }

    if (canvas.height !== h) {
        canvas.height = h;
    }
}

function init() {
    window.addEventListener("resize", resize, false);
    canvas.addEventListener("mousedown", md);
    canvas.addEventListener("mousemove", mm);
    canvas.addEventListener("mouseup", mu);
    document.body.appendChild(canvas);

    ctx.lineJoin = ctx.lineCap = 'round';

    resize();
}

function point(x, y) {
    return {
        x: x,
        y: y
    }
}

function md(event) {
    isDragging = true;

    var p = point(event.clientX, event.clientY);
    points.push(p);
    rgb = [rand(255, 0), rand(255, 0), rand(255, 0)]
}

function rand(max, min) {
    min = min || 0;
    return ~~(Math.random() * (max - min)) + min
}

function dis(from, to) {
    var dx = to.x - from.x;
    var dy = to.y - from.y;

    return {
        dx: dx,
        dy: dy,
        d: Math.sqrt(dx * dx + dy * dy)
    }
}

function mm(event) {
    if (!isDragging)
        return;

    var p = point(event.clientX, event.clientY);
    var p1 = points[points.length - 1 | 0];
    points.push(p);

    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = "rgba("
        + rgb
        + ", .2)";
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p.x, p.y);
    ctx.stroke();
    ctx.closePath();

    var l = points.length - 1
        , r
        ;

    ctx.beginPath();

    while (l--) {
        p1 = points[l];
        r = dis(p, p1);

        if (r.d < step) {

            ctx.lineWidth = 2;
            ctx.fillStyle = "rgba(" + [rand(255, 0), rand(255, 0), rand(255, 0)]  + ", .05)";
            ctx.strokeStyle = "rgba("
                + rgb
                + ", .2)";
            ctx.strokeStyle = 'hsla(' + rgb[0] + ',100%,50%, .05)';
            if (true) {
                ctx.moveTo(p.x + (r.dx * .2), p.y + (r.dy * .2));
                ctx.lineTo(p1.x - (r.dx * .2), p1.y - (r.dy * .2));
            }
            else {
                ctx.lineTo(p1.x, p1.y);
            }

        }
    }

    ctx.closePath();
    ctx.stroke();
    ctx.fill();
}

function mu() {
    isDragging = false;
    points.length = 0;
}

var ticks = 0;
var target;
var last;
var radius = 0;
var radiusDelta = 60;
d3.timer(function move() {

    target = target || {x: w/2, y: h/2};
    last = last || target;

    var r = dis(last, target).d;

    if (r < 1) {
        ticks += radiusDelta;
        target = {
            x: w/2 + radius * Math.cos(ticks * Math.PI / 180),
            y: h/2 + radius * Math.sin(ticks * Math.PI / 180)
        };
        if (!(ticks%360)) {
            rgb = [rand(255, 0), rand(255, 0), rand(255, 0)];
            radius += step * .5;
            radiusDelta += 30;
            if (radiusDelta > 360) {
                radiusDelta = 30;
            }
        }
        /*ctx.save();
         ctx.fillStyle = "red";
         ctx.arc(target.x, target.y, 5, 0, Math.PI*2);
         ctx.fill();
         ctx.restore();*/
    }

    var dx = target.x - last.x;
    var dy = target.y - last.y;

    isDragging = true;
    canvas.dispatchEvent(new MouseEvent("mousemove", {
        bubbles: true,
        cancelable: true,
        view: window,
        clientX: (last.x += dx * .1),
        clientY: (last.y += dy * .1)
    }));

    //setTimeout(move, 100);
    return false;
});

init();